$fa=1;
$fs=0.4;

//splitter(z=1.8)
keycap();

module splitter(z, u=0.3) {
  union() {
    translate([0, 0, z])
    rotate([0, 180, 0])
    intersection() {
      children();
      cube([20, 20, 2*z], center=true);
    }

    translate([0, 16.7, -z])
    rotate([0, 0, 180])
    difference() {
      children();
      cube([20, 20, 2*z], center=true);
    }

    translate([0, 10, u/2])
    cube([10, 10, u], center=true);
  }
}

module keycap() {
  difference() {
    import("MBK_Keycap_-_1u.stl", convexity=3);

    translate([0, 0, 16.32])
    rotate([94, 0, 0])
    translate([0, 0, 5])
    cylinder(d=27, h=14, center=true);

    translate([0, -3, 16.32])
    rotate([98, 0, 0])
    translate([0, 0, 5])
    cylinder(d=27, h=14, center=true);

    narrow();
    narrow(-1);
    //translate([0, -2, 21])
    //sphere(d=38);
  }
}

module narrow(f=1) {
  rotate([0, 0, -f*7.5])
  translate([f*10.5, 0, 0])
  cube([4, 30, 10], center=true);
}


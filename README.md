# MBK keycaps and modifications

I just modify `MBK_Keycap_-_1u.stl` for a confort use with the thumb, using `thumb.scad`.

## Thirth party models

Some models were downloaded from the following links under the [Creative Commons - Attribution -
Non-Commercial](https://creativecommons.org/licenses/by-nc/4.0/) license:
- https://www.thingiverse.com/thing:4564253/files
  - https://cdn.thingiverse.com/assets/75/d6/f3/b8/52/MBK_Keycap_-_1u.stl
  - https://cdn.thingiverse.com/assets/85/2b/02/79/bc/MBK_Keycap_-_1u_homing.stl
  - https://cdn.thingiverse.com/assets/87/aa/00/c0/c5/MBK_Keycap_-_1.5u.stl
  - https://cdn.thingiverse.com/assets/db/8f/70/12/d4/MBK_Keycap_-_2u.stl

Thanks to [darryldh](https://www.thingiverse.com/darryldh).
